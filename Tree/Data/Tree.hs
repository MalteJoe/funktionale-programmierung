{-# LANGUAGE DeriveDataTypeable #-}

-- ----------------------------------------

-- | binary tree with values at the leafs (Tip),
-- the branches (Bin) don't contain any further information,
-- the empty tree is represented by a special value Null

module Data.Tree
where

import           Prelude             hiding (foldl, foldr, head, tail, init, last)

import           Control.Applicative
import           Control.Monad

import           Data.Data
import           Data.Foldable      hiding (toList)
import           Data.Monoid

-- ----------------------------------------

data Tree a
    = Null
    | Tip a
    | Bin (Tree a) (Tree a)
      deriving (Show, Data, Typeable)

-- | smart constructor
bin :: Tree a -> Tree a -> Tree a
bin left Null = left
bin Null right = right
bin left right = Bin left right

-- class Functor f where
--  fmap :: (a -> b) -> (f a -> f b)
instance Functor Tree where
  fmap f Null = Null
  fmap f (Tip x) = Tip (f x)
  fmap f (Bin x y) = Bin (fmap f x) (fmap f y)

-- class Applicative m where
--   pure :: a -> m a
--   <*> :: f( a -> b) -> f a -> f b
instance Applicative Tree where
  pure x = Tip x
  Null <*> _ = Null
  (Tip f) <*> x = fmap f x
  (Bin l r) <*> x = Bin (l <*> x) (r <*> x)

-- class Monad m where
--   return :: a -> m a
--   >>= :: m a -> (a -> m b) -> m b
instance Monad Tree where
  return     = Tip
  Null    >>= f = Null
  (Tip x) >>= f = f x
  (Bin l r) >>= f = Bin (l >>= f) (r >>= f)

instance Alternative Tree where
  empty = mzero   -- or Null
  (<|>) = mplus

-- class MonadPlus m 
--   mzero :: m a
--   mplus :: m a -> m a -> m a
instance MonadPlus Tree where
  mzero = Null
  mplus Null t = t
  mplus t _ = t 

-- class Monoid m where
--   mempty :: a
--   mappend :: a -> a -> a
instance Monoid (Tree a) where
  mempty  = Null
  mappend l r = bin l r

-- fold elements like in a list from right to left
-- Baum in List konvertieren und darauf foldr anwenden
instance Foldable Tree where
  foldr f e Null = e
  foldr f e (Tip a) = f a e
  foldr f e (Bin l r) = foldr f (foldr f e r) l

-- ----------------------------------------
-- classical visitor

visitTree :: b -> (a -> b) -> (b -> b -> b) -> Tree a -> b
visitTree e tf bf = visit'
  where
    visit' Null = e
    visit' (Tip a) = tf a
    visit' (Bin left right) = bf (visit' left) (visit' right)
    
-- special visitors

sizeTree :: Tree a -> Int
sizeTree = visitTree 0 (const 1) (+)
 
-- sizeTree trivial
--sizeTree tree = sum (map (const 1) (toList tree))

minDepth, maxDepth :: Tree a -> Int
minDepth = visitTree 0 (const 1) (\ x y -> (min x y)+1)

-- minDepth = visitTree 0 (const 1) ((+1) . min)
-- ((+1) . min) :: (Num (a -> a), Ord a) => a -> a -> a
-- (\ x y -> (min x y)+1) :: (Num a, Ord a) => a -> a -> a

maxDepth = visitTree 0 (const 1) (\ x y -> (max x y)+1)

-- ----------------------------------------
-- access functions

viewL :: Tree a -> Maybe (a, Tree a)
viewL Null = Nothing
viewL (Tip x) = Just (x, Null)
viewL (Bin (Tip x) r) = Just (x, r)
viewL (Bin l r) = Just (x, (bin res r))
    where
    Just (x,res) = viewL l

viewR :: Tree a -> Maybe (Tree a, a)
viewR Null = Nothing
viewR (Tip x) = Just (Null, x)
viewR (Bin l (Tip x)) = Just (l, x)
viewR (Bin l r) = Just ((bin l res), x)
    where
    Just (res,x) = viewR r

head :: Tree a -> a
head = maybe (error "head: empty tree") fst . viewL

tail :: Tree a -> Tree a
tail = maybe (error "tail: empty tree") snd . viewL

last :: Tree a -> a
last = maybe (error "last: empty tree") snd . viewR

init :: Tree a -> Tree a
init = maybe (error "init: empty tree") fst . viewR

-- ----------------------------------------
-- conversions to/from lists

-- | runs in O(n) due to the use of (:)
toList :: Tree a -> [a]
toList = foldr (:) []

-- | runs in O(n^2) due to the use of (++)
toListSlow :: Tree a -> [a]
toListSlow = visitTree [] (\ x -> [x]) (\ l r -> l ++ r)

-- | build a balanced tree
--
-- doesn't work for infinite lists

-- weak balancing criterion
fromList :: [a] -> Tree a
fromList [] = Null
-- fromList (x:xs) = insert x tree
--    where
    --insert :: b -> Tree a -> Tree a
--    insert e Null = Tip e
--    insert e (Tip old) = bin (Tip old) (Tip e)
--    insert e (Bin l r) = if (minD == maxD) then bin tree (Tip e) else bin l (insert e r)
--    tree = fromList xs
--    minD = minDepth tree
--    maxD = maxDepth tree

-- Lösung Schmidt
fromList xs = merge $ map Tip xs
    where
    merge :: [Tree a] -> Tree a
    merge [t] = t
    merge ts = merge $ pair ts
    
    pair (t1:t2:ts') = bin t1 t2 : pair ts'
    pair ts' = ts'
    
-- strong balancing criterion - höchsten 1 Unterschied
-- split at (Liste immer in der Mitte Splitten (dann muss man allerdings mehrmals durch die List durch
fromList' :: [a] -> Tree a
fromList' (x:[]) = Tip x
fromList' list = Bin (fromList' l) (fromList' r)
    where
    (l,r) = splitAt ((length list) `div` 2) list

-- list to the right
fromList'' :: [a] -> Tree a
fromList'' = foldr (\ x t -> Tip x `bin` t) Null

-- list to the left
fromList''' :: [a] -> Tree a
fromList''' = foldl (\ t x -> t `bin` Tip x) Null

-- runtime differences between fromList, fromList', fromList'', fromList'''?
-- differences in balancing quality?

-- ----------------------------------------
