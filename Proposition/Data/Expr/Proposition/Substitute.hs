module Data.Expr.Proposition.Substitute where

import Data.List      (union)

import Data.Expr.Proposition.Types

-- ----------------------------------------
-- variable substitution

type VarEnv = [(Ident, Expr)]

substVars :: VarEnv -> Expr -> Expr
substVars env (Lit b) = Lit b
substVars env (Var i) = case lookup i env of
                            Nothing -> Var i
                            (Just x) -> x
substVars env (Unary op e1) = Unary op (substVars env e1)
substVars env (Binary op e1 e2) = Binary op (substVars env e1) (substVars env e2)

freeVars :: Expr -> [Ident]
freeVars  (Lit b) = []
freeVars (Var i) = [i]
freeVars (Unary op e1) = freeVars e1
freeVars (Binary op e1 e2) = (freeVars e1) ++ (freeVars e2)

-- ----------------------------------------
