-- | the visitor pattern for Expr

module Data.Expr.Proposition.Visitors where

import Data.Expr.Proposition.Types
import Data.Expr.Proposition.Visit
import Data.Expr.Proposition.Eval (mf1, mf2)

import Data.Set(Set)
import qualified Data.Set as S

-- ----------------------------------------

type Idents = Set Ident

freeVars :: Expr -> Idents
freeVars = visit $ V {vLit = const S.empty,
                      vVar = S.singleton,
                      vUnary = const id,
                      vBinary = const S.union
                      }
    
type VarEnv = [(Ident, Expr)]

substVars :: VarEnv -> Expr -> Expr
substVars env = visit $ idExpr {
                        vVar = \ k ->
                                case lookup k env of
                                    Nothing -> Var k
                                    (Just v) -> v
                      }

eval :: Expr -> Bool
eval
  = visit $ V {
                vLit = id,
                vVar = \x -> error ("Variable " ++ x ++ " undefined"),
                vUnary = mf1,
                vBinary = mf2
                }

-- ----------------------------------------
