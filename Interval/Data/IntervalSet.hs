module Data.IntervalSet
where

-- ----------------------------------------

-- an pair of Ints can represent closed Intervals
-- (i, j) <=> [i..j]
-- Intervalls with i > j represent the empty set

type Interval = (Int, Int)

overlap :: Interval -> Interval -> Bool
overlap (x1, y1) (x2, y2)
  | x1 <= x2 = y1 >= x2-1 
  | otherwise = y2 >= x1-1


less :: Interval -> Interval -> Bool
less (_x1, y1) (x2, _y2)
  = y1 < x2

                           
emptyInterval :: Interval -> Bool
emptyInterval (x, y) = x > y


-- merge 2 (overlapping) intervals
merge :: Interval -> Interval -> Interval
merge (x1, y1) (x2, y2) = (min x1 x2, max y1 y2)


-- ----------------------------------------

-- a set of integers can be represented by an
-- ordered list of none empty intervals, which
-- do not overlap

type IntervalSet = [Interval]

inv :: IntervalSet -> Bool
inv [] = True
inv is = and (map (not . emptyInterval) is)
    && and (zipWith testPair is (tail is))
    where testPair i1 i2 = not (overlap i1 i2) && less i1 i2
        


-- ----------------------------------------
-- internal interval set ops

singleInterval :: Int -> Int -> IntervalSet
singleInterval x y
    | x <= y    = [(x, y)]
    | otherwise = []

insertInterval :: Interval -> IntervalSet -> IntervalSet
insertInterval new [] = [new]
insertInterval new (i:is)
    | overlap new i = insertInterval (merge new i) is
    | less new i = new:i:is
    | otherwise = i:(insertInterval new is)


fromIntervalList :: [(Int, Int)] -> IntervalSet
fromIntervalList = foldr insertInterval [] 
-- foldr union empty . map (uncurry singleInterval)


-- ----------------------------------------
--
-- exported ops, names similar to Data.Set

empty :: IntervalSet
empty = []

singleton :: Int -> IntervalSet
singleton i = singleInterval i i

insert :: Int -> IntervalSet -> IntervalSet
insert i = insertInterval (i, i)

union :: IntervalSet -> IntervalSet -> IntervalSet
union first = foldr (\ i is -> insertInterval i is) first


member :: Int -> IntervalSet -> Bool
member x = foldr ((||) . isIn x) False
    where 
    isIn x i = (fst i) <= x && (snd i) >= x

         
fromList :: [Int] -> IntervalSet
fromList = foldr insert []


toList :: IntervalSet -> [Int]
toList = foldr (\ (s,e) list -> [s..e] ++ list ) []


-- ----------------------------------------
