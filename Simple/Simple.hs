module Simple
where

-- Definieren Sie eine Funktion fib zur Berechung der Fibonacci-Zahlen
-- ab 0 
fib     :: Integer -> Integer
fib 0 = 0
fib 1 = 1
fib x | x < 0  = error "negative argument"
      | x > 1  = fib (x-1) + fib (x-2) 

-- Definieren Sie eine Funktion fib zur Berechung der Fibonacci-Zahlen
-- ab 0 mit linearer Laufzeit

fib2    :: Integer -> Integer
fib2 0 = 0
fib2 1 = 1
fib2 x = fib' 0 1 x
    where
        fib' n0 n1 x
            | x == 0 = n0
            | x >  0 = fib' n1 (n0 + n1) (x-1)

-- Definieren Sie eine Funktion c (für Collatz), die berechnet
-- wie viele Rekursionsschritte benötigt werden, um
-- eine natürliche Zahl n >= 1 auf 1 zu
-- reduzieren.
--
-- Folgende Reduktionsregel sind dabei anzuwenden: Wenn n gerade ist,
-- so wird n halbiert, wenn n ungerade ist, so wird n verdreifacht und um
-- 1 erhöht.
    
c       :: Integer -> Integer
c n 
    | n == 1 = 0
    | even n    = 1 + c (n `div` 2)
    | otherwise = 1 + c (n*3 + 1)


-- Definieren Sie ein endrekurive Variante von c
    
c1      :: Integer -> Integer
c1 n = c1' n 0
    where
        c1' n i
            | n == 1 = i
            | even n    = c1' (n `div` 2) (i+1)
            | otherwise = c1' (n*3 + 1) (i+1)


-- Definieren Sie eine Funktion cmax, die für ein
-- Intervall von Zahlen das Maximum der
-- Collatz-Funktion berechnet. Nutzen Sie die
-- vordefinierten Funkt min und max.

cmax    :: Integer -> Integer -> Integer
cmax lb ub
    | lb > ub = error "Illegal Argument"
    | lb == ub = c1 lb
    | otherwise = max (cmax (lb+1) ub) (c1 lb)


-- Definieren Sie eine Funktion imax, die für ein
-- Intervall von Zahlen das Maximum einer
-- ganzzahligen Funktion berechnet. Formulieren
-- Sie die obige Funktion cmax so um, dass sie mit imax arbeitet.

imax    :: (Integer -> Integer) -> Integer -> Integer -> Integer
imax f lb ub
    | lb > ub = error "Illegal Argument"
    | lb == ub = f lb
    | otherwise = max (imax f (lb+1) ub) (f lb)

cmax1   :: Integer -> Integer -> Integer
cmax1
    = imax c1

-- Entwickeln Sie eine Funktion,
-- die die Position und den Wert bestimmt, an der
-- das Maximum angenommen wird.
-- Versuchen Sie, eine endrekursive Lösung zu finden
-- (mit einer lokalen Hilfsfunktion).

imax2   :: (Integer -> Integer) -> Integer -> Integer -> (Integer, Integer)
imax2 f lb ub
    | lb > ub = error "Illegal Argument"
    | lb == ub =  (lb, res)
    | otherwise = if val > res then (element, val) else (lb, res)
    where 
        (element, val) = imax2 f (lb+1) ub
        res = f lb

-- ----------------------------------------
