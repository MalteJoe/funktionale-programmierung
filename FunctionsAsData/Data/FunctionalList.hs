module Data.FunctionalList
where

import           Prelude (Bool(..), (.), (++), undefined)
import qualified Prelude as P

type List a = [a] -> [a]

-- ----------------------------------------

-- Liefert eine Funktion, über die man eine weitere Liste vor die Liste hängen kann
fromList        :: [a] -> List a -- [a] -> ([a] -> [a])
fromList l      = \ x -> x ++ l

-- liefert den ersten Listenparameter der Funktion fromList
toList          :: List a -> [a] -- ([a] -> [a]) -> [a]
toList l        = l []

empty           :: List a
empty           = fromList []

singleton       :: a -> List a
singleton e     = fromList [e]
-- P.const [e]

-- (:) for functional lists
cons            :: a -> List a -> List a
cons e l        = \ x -> e:(l x)

-- dual to cons
snoc            :: List a -> a -> List a
-- snoc l e        = \ x -> (l x) ++ [e]
snoc l e        = append l (singleton e)

-- (++) for functional lists
append          :: List a -> List a -> List a
-- l1 vor l2 Hängen, wenn das dann mit der [] aufgerufen wird, erhalten wir l1 ++ l2
-- append l1 l2 = fromList ((toList l1) ++ (toList l2))
append l1 l2 = l2 . l1

-- like concat for normal lists: foldr (++) []
concat          :: [List a] -> List a
concat          = P.foldr append empty

-- like map for normal lists: foldr ((:) . f) []
map             :: (a -> b) -> List a -> List b
-- map f l1        = fromList (P.map f (toList l1))
map f           = foldr (cons . f) empty

-- foldr with foldr for normal lists
foldr           :: (a -> b -> b) -> b -> List a -> b
foldr op n      = P.foldr op n . toList

-- head, tail, null
head            :: List a -> a
head            = P.head . toList

tail            :: List a -> List a
tail l            = P.tail . l

null            :: List a -> Bool
null            = P.null . toList

reverse         :: List a -> List a
reverse l        = P.reverse . l
-- reverse = (P.reverse .)

-- ----------------------------------------
